
Hi,


Welcome to  mpd.warp, a set of  perl scripts I have  written for music
player daemon.  This tarball contains the following scripts:




_____ mpd.crontab.shuffle.pl _________________________________________

A crontab version of an album shuffle function.

The way this works is, mpd.crontab.shuffle will check if enough tracks
are  currently  queued  --  whenever  this  drops  below  a  specified
threshold (e.g. 2 hours) it will  randomly pick an album from a set of
predefined directories, and add it to your playlist.

Use the  crontab.shuffle script to set  a list of  directories to pick
albums from,  and to make sure it  adds enough tracks each  time it is
called.




___ crontab.shuffle __________________________________________________

A  little  wrapper  for   mpd.crontab.shuffle.pl,  edit  the  list  of
directories in this file, and put it in your crontab.




___ mpd.clean.pl _____________________________________________________

This can  be used in  addition to mpd.crontab.shuffle, it  will remove
all the albums which have already been played from the playlist.




___ mpd.queue.pl _____________________________________________________

mpd.queue is a script to enqueue tracks/albums/directories at a 
convenient spot in the current playlist.

use "mpd.queue.pl <tracks to add>" to queue tracks after the currently
playing album.

use "mpd.queue.pl --now  <tracks to add>" to queue  tracks right after
the currently playing track.

This is  a very  useful way to  queue stuff if  mpd.crontab.shuffle is
running, but is obviously useful on its own too.




___ MPD_warp.pm ______________________________________________________

Originally intended as a module  to contain functions common to my mpd
scripts,  it is  currently only  used by  mpd.queue.pl.  Just  keep it
around in the same directory where these scripts are stored.




___ MPD.pm ___________________________________________________________

A copy of the MPD.pm module is included, so you don't have to fetch it
seperately.  If you already have a MPD.pm installed, you can remove
the one packaged here.

At the time of writing this README, the included MPD.pm is 0.12.0-rc6,
from http://mercury.chem.pitt.edu/~shank/MPD.pm-0.12.0-rc6.tar.gz.

Look at http://www.musicpd.org/MPD-pm.shtml for more information.

