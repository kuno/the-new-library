#!/usr/bin/perl -w
#
# mpd.crontab.shuffle.pl, an album shuffle hack for music player daemon.
# Copyright (c) 2005  Kuno Woudt <kuno@frob.nl>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

use strict;
use Data::Dumper;

use FindBin;
use lib $FindBin::Bin;

use MPD;

my $app = "mpd.crontab.shuffle.pl";
my $version = "0.0.2";
my $copyright = 'Copyright (C) 2005  Kuno Woudt <kuno@frob.nl>';

sub version
  {
    print $app." ".$version."\n".$copyright."\n\n";
    exit 1;
  }

sub help
  {
    print $app." ".$version."\n".$copyright."\n\n";
    print "Usage: ".$app." [OPTIONS] [TRACKS]\n\n"
      ."\t--help   \tdisplay this help and exit\n"
      ."\t--version\tdisplay version information and exit\n"
      ."\n";

    exit 1;
  }


sub get_albums
  {
    my $x = shift;
    my @list;

    die ("please specify atleast one directory.\n") if ($#_ < 0);

    foreach my $dir (@_)
      {
        foreach ($x->listall($dir))
          {
            if ("file" eq substr $_, 0, 4)
              {
                s,/[^/]*$,,;
                s,^......,,;

                push @list, $_ if ($#list < 0 or $list[$#list] ne $_);
              }
          }
      }

    return @list;
  }

sub queue_something
  {
    my $x = shift;
    our @album_list;

    my $r = int (rand ($#album_list+1));

#    print "random selection: ".$r.", which is ".$album_list[$r].".\n";

    $x->add ($album_list[$r]);
  }

sub remaining_time
  {
    my $x = shift;

    my $total;
    my $playlist = $x->playlist;

    for (my $i = $x->{song} ? $x->{song} : 0;
         $i < $x->{playlistlength}; $i++)
      {
        $total += $playlist->[$i]{'Time'};
      }

    my($psf,$tst) = split /:/, $x->{'time'};

    return $total-$psf;
  }

my $x = MPD->new();

&help if (defined ($ARGV[0]) and $ARGV[0] eq "--help");
&version if (defined ($ARGV[0]) and $ARGV[0] eq "--version");

die ("please specify a threshold.\n") if (!defined ($ARGV[0]));

my $threshold = shift @ARGV;

my $seconds = remaining_time ($x);
print "remaining time: ".$seconds." seconds (threshold is ".$threshold.").\n";

if ($seconds < $threshold)
  {
    our @album_list = get_albums ($x, @ARGV);
    queue_something ($x);

    exit 0;
  }

exit 1;


