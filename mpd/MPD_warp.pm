#!/usr/bin/perl -w
#
# MPD_warp.pm, mpd client functions common to my mpd scripts.
# Copyright (C) 2005  Kuno Woudt <kuno@frob.nl>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

package MPD_warp;

use strict;
use Data::Dumper;

use FindBin;
use lib $FindBin::Bin;

use MPD;

use constant VERSION => '0.0.1';

sub _add
  {
    my $x = shift;

    my $len = $x->{playlistlength};

    $x->add ($_) foreach (@_);

    # force update of {playlistlength}
    $x->_get_status ();
    my $newlen = $x->{playlistlength};

    return $newlen-$len;
  }


sub _now
  {
    my $x = shift;
    my $verbose = shift;

    print "queueing after track \"".$x->get_title."\".\n" if ($verbose);

    return $x->{song}+1;
  }

sub _soon
  {
    my $x = shift;
    my $verbose = shift;
    my $playlist = $x->playlist;

    my $current;
    my $i = $x->{song} ? $x->{song} : 0;
    my $album = $playlist->[$i]{'Album'};
    while (++$i < $x->{playlistlength})
      {
        last if ($album ne $playlist->[$i]{'Album'});
      }

    print "queueing after album \"".$album."\".\n" if ($verbose);

    return $i;
  }


sub queue
  {
    my $x = shift;
    my $now = shift;
    my $verbose = shift;

    my $len = $x->{playlistlength};
    my $count = _add ($x, @_);
    my $current = $now ? _now ($x, $verbose) : _soon ($x, $verbose);

    $x->move ($len++, $current++) foreach (1..$count);
  }

1;
