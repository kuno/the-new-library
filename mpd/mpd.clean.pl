#!/usr/bin/perl -w
#
# mpd.clean.pl, cleans stuff from the playlist which has been played.
# Copyright (C) 2005  Kuno Woudt <kuno@frob.nl>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

use strict;
use Data::Dumper;

use FindBin;
use lib $FindBin::Bin;

use MPD;

my $app = "mpd.clean.pl";
my $version = "0.0.1";
my $copyright = 'Copyright (C) 2005  Kuno Woudt <kuno@frob.nl>';

sub version
  {
    print $app." ".$version."\n".$copyright."\n\n";
    exit 1;
  }

sub help
  {
    print $app." ".$version."\n".$copyright."\n\n";
    print "Usage: ".$app." [OPTIONS]\n\n"
      ."\t--help   \tdisplay this help and exit\n"
      ."\t--version\tdisplay version information and exit\n"
      ."\n";

    exit 1;
  }

sub clean
  {
    my $x = shift;
    my $verbose = shift;
    my $playlist = $x->playlist;

    my $current;
    my $i = -1;
    my $end = $x->{song} ? $x->{song} : 0;
    my $album = $playlist->[$end]{'Album'};
    while (++$i <= $end)
      {
        last if ($album eq $playlist->[$i]{'Album'});
      }

    print "removing ".$i." tracks from playlist.\n";

    $x->delete(0) foreach (1..$i);

    return $i;
  }



my $x = MPD->new();

&help if (defined ($ARGV[0]) and $ARGV[0] eq "--help");
&version if (defined ($ARGV[0]) and $ARGV[0] eq "--version");

clean ($x);

