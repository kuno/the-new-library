#!/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import couchdb
import json
import os
import os.path
import re
import sys

couch = couchdb.Server ()
db = couch["lud"]

if not "lud.queue" in db:
    db["lud.queue"] = { "playlist": [] }

doc = db["lud.queue"]
for item in doc["playlist"]:
    print (item)

