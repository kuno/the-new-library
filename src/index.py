#!/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import couchdb
import json
import os
import os.path

couch = couchdb.Server ()
db = couch["lud"]

music_root = os.path.expanduser("~/music")
music_root = "/mnt/zooyork/annex/library/"
music_url = "https://18-volt.frob.mobi/m593/"

for root, dirs, files in os.walk (music_root):
    # Skip hidden files and directories.
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not d[0] == '.']

    if not "metadata.json" in files:
        continue

    filename = os.path.join (root, "metadata.json")
    with codecs.open (filename, "rb", "utf-8") as f:
        data = json.loads (f.read ())
        id = data["id"]
        data["_id"] = id
        data["lud_path"] = os.path.abspath (root)
        data["lud_url"] = data["lud_path"].replace (music_root, music_url) + "/"

        data["lud_disc_urls"] = sorted([
            data["lud_url"] + x
            for x in files
            if x.startswith("disc") and x.endswith(".cue")
        ])

        if id in db:
            in_couch = db[id]
            rev = in_couch.pop("_rev")
            if json.dumps(in_couch, sort_keys=True) == json.dumps(data, sort_keys=True):
                print (id, "is unchanged, not updating")
                continue

            data["_rev"] = rev

        print ("Indexing", data["_id"], "at", data["lud_path"])
        db[id] = data

# for id in db:
#     print (id)
