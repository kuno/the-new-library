#!/bin/env python3
#
# Copyright 2017  Kuno Woudt
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1, see ../copyleft-next-0.3.1.txt
#

import codecs
import json
# import re
import requests
import os
import sys
# import unicodedata
from os.path import abspath, dirname, isdir, isfile, join

here = join (dirname (dirname (abspath (__file__))))
get_metadata_cmd = join (here, 'bin', 'release-metadata')
metadata_dir = join (here, '_metadata')

os.makedirs(metadata_dir, exist_ok=True)

mbid = sys.argv[1]

release_json = join (metadata_dir, 'release.' + mbid + '.jsonld')
cover_url = "http://coverartarchive.org/release/" + mbid + "/front-500.jpg"
cover_jpg = join (here, 'assets', mbid + '.jpg')

if not os.path.exists (release_json):
    os.system (get_metadata_cmd + ' ' + mbid)

if not os.path.exists (release_json):
    print ("Error downloading metadata")
    sys.exit (1)

if not os.path.exists (cover_jpg):
    response = requests.get (cover_url)
    with open (cover_jpg, "wb") as f:
        f.write (response.content)

with codecs.open (release_json, "rb", "utf-8") as f:
    data = json.loads (f.read ())

    country = ""
    year = ""
    if "hasReleaseRegion" in data:
        year = data["hasReleaseRegion"][0]["releaseDate"][0:4]
        country = data["hasReleaseRegion"][0]["releaseCountry"]["name"]

    if country == "[Worldwide]":
        country = ""

    artist = data["creditedTo"]
    duration = data["duration"].replace("PT", "").replace("H", "h ").replace("M", "m ").replace("S", "s ")
    values = {
        "artist": artist,
        "cover_url": "/blog/assets/" + mbid + ".jpg",
        "release_url": "https://musicbrainz.org/release/" + mbid,
        "title": data["name"],
    }

    details = []
    if country:
        details.append('<span class="country">%s</span>' % country)

    if year:
        details.append('<span class="year">%s</span>' % year)

    if duration:
        details.append('<span class="duration">%s</span>' % duration)

    values["details"] = " &middot; ".join (details)

    output = """
 <span class="music">
     ![%(title)s](%(cover_url)s)
     <span class="data">
         <span class="title">[%(title)s](%(release_url)s)</span><br />
         <span class="artist">by %(artist)s</span><br />
         %(details)s
     </span>
 </span>
 """ % values

    print (output)
