#!/bin/sh

DST=disc1
if [[ `pwd` =~ ^.*/(disc[0-9]+)$ ]]; then
    DST=${BASH_REMATCH[1]}
fi

ls *.ogg | sed 's/^/oggdec "/' | sed 's/$/"/' | sh
rename -v 's/^([0-9]*)\..*.wav/$1.wav/' *wav
shntool cue *.wav > "$DST.cue"
shntool len *.wav > shntool.len
cat *.ogg > tmp.ogg
ffmpeg -i tmp.ogg -vn -strict experimental -acodec aac -b:a 256k "$DST.m4a"


