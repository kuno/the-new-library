#!/bin/sh

DST=disc1
if [[ `pwd` =~ ^.*/(disc[0-9]+)$ ]]; then
    DST=${BASH_REMATCH[1]}
fi

IFS='.'
for f in *.m4a; do
    read -ra FILENAME <<< "$f"
    ffmpeg -i "$f" "${FILENAME[0]}.wav"
done

ls *.wav  | sed "s/^/file '/" | sed "s/$/'/" > list.txt
shntool cue *.wav > "$DST.cue"
shntool len *.wav > shntool.len
ffmpeg -f concat -i list.txt -vn -strict experimental -acodec aac -b:a 256k "$DST.m4a"
