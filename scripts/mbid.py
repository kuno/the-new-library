#!/usr/bin/env python3

import os.path
import shlex
import sys
from collections import namedtuple
from mutagen.flac import FLAC
from mutagen.oggvorbis import OggVorbis
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4

ParseResults = namedtuple("ParseResults", ["mbid", "discno"])

def albumid (metadata):
    """ retrieves the musicbrainz albumid from the file. """

    key = 'musicbrainz_albumid'
    mp3 = 'TXXX:MusicBrainz Album Id'
    mp4 = '----:com.apple.iTunes:MusicBrainz Album Id'

    if key in metadata:
        return metadata[key][0]
    if mp3 in metadata:
        return metadata[mp3].text[0]
    if mp4 in metadata:
        return metadata[mp4][0].decode ()

    return ''


def discno (metadata):
    if "TPOS" in metadata:
        disc_str = metadata["TPOS"].text[0]
        parts = disc_str.split("/")
        return int(parts[0])

    if "discnumber" in metadata:
        return int(metadata["discnumber"][0])

    if "disk" in metadata:
        return int(metadata["disk"][0][0])

    return 1


def _metadata (file):
    """ retrieves the lowlevel metadata from a file for further processing by other functions. """

    if file.lower ().endswith ('.flac'):
        return FLAC (file)
    elif file.lower ().endswith ('.ogg'):
        return OggVorbis (file)
    if file.lower ().endswith ('.mp3'):
        return MP3 (file)
    if file.lower ().endswith ('.mp4'):
        return MP4 (file)
    if file.lower ().endswith ('.m4a'):
        return MP4 (file)
    else:
        return None


def parse (file):
    """ parse the metadata in a file, and return it. """

    meta = _metadata (file)
    if meta == None:
        return None

    return ParseResults(albumid (meta), discno (meta))


def mbid_for_dir (dir):
    files = os.listdir (dir)
    files.sort ()

    for filename in files:
        if (filename.endswith (".flac")
            or filename.endswith (".ogg")
            or filename.endswith (".mp4")
            or filename.endswith (".m4a")
            or filename.endswith (".mp3")):
            # print ("parse", os.path.join (dir, filename))
            result = parse (os.path.join (dir, filename))

            if result:
                return result

    return None


if __name__ == "__main__":
    if len (sys.argv) == 2:
        os.chdir (sys.argv[1])

    result = mbid_for_dir (".")
    if result:
        print (result.mbid, result.discno)
        sys.exit (0)

    sys.exit (1)

