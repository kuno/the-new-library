#!/usr/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import json
import mbid
import os
import os.path
import requests
import sys
import collections
import re

def parse_cue (f):
    lines = f.readlines ()
    data = { "_current": None, "tracks": [] }
    for line in lines:
        if line.startswith ("FILE"):
            parts = line.split (' ', maxsplit=1)
            args = parts[1].rsplit (' ', maxsplit=1)
            value = args[0].replace('"', '')
            return value

    return None


def verify_cue (dir, cue_filename):
    full_cue_filename = os.path.join (dir, cue_filename)

    with codecs.open (full_cue_filename, "rb", "utf-8") as f:
        audio_filename = parse_cue (f)
        full_audio_filename = os.path.join (dir, audio_filename)
        if os.path.isfile (full_audio_filename):
            return audio_filename
        else:
            print ("WARNING:", full_audio_filename, "not found")
            return None


def clean (dir, files):
    found_disc = False

    keep_files = ["cover.jpg", "metadata.json"]

    for filename in files:
        m = re.match("^disc([0-9]+).cue$", filename)
        if not m:
            continue

        audio_filename = verify_cue (dir, filename)
        if not audio_filename:
            return False

        keep_files.append (filename)
        keep_files.append (audio_filename)

        found_disc = True

    if not found_disc:
        print ("WARNING: no disc found in", dir)
        return False

    if not "front.jpg" in files and not "cover.jpg" in files:
        print ("WARNING: no cover art found in", dir)
        return False

    if not "cover.jpg" in files:
        src = os.path.join (dir, "front.jpg")
        dst = os.path.join (dir, "cover.jpg")
        os.rename (src, dst)

    for filename in files:
        if not filename in keep_files:
            os.unlink (os.path.join (dir, filename))

    print ("cleaned", dir)
    return True


dirs = ["."]
if len (sys.argv) > 1:
    dirs = sys.argv[1:]

dirs = list (map (os.path.realpath, dirs))

for current_dir in dirs:
    if os.path.isdir (current_dir):
        os.chdir (current_dir)
        for root, dirs, files in os.walk (current_dir):
            if "metadata.json" in files:
                clean (root, files)
