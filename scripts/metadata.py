#!/usr/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import collections
import json
import mbid
import os
import os.path
import requests
import sys
import time
import re

cache = {}

def artist_name (ac):
    name = []
    for part in ac:
        name.append (part["name"])
        name.append (part["joinphrase"])

    return "".join (name)

def download_metadata (release_id):
    metadata_str = ""
    if release_id in cache:
        metadata_str = cache[release_id]
    else:
        url = ("https://musicbrainz.org/ws/2/release/"
               + release_id
               + "?fmt=json&inc=artist-credits+media+recordings+discids+isrcs")

        print ("Requesting metadata for", release_id)
        time.sleep (1)
        response = requests.get (url)
        if not response.status_code == 200:
            print ("ERROR [%d]: could not download release metadata"
                   % response.status_code, file=sys.stderr)
            print (response.text, file=sys.stderr)
            sys.exit (1)

        metadata = response.json()
        if not "title" in metadata:
            print ("ERROR: title missing from metadata", file=sys.stderr)
            print ("ERROR: " + url, file=sys.stderr)
            sys.exit (1)

        metadata_str = json.dumps (metadata, sort_keys=True, indent=4) + "\n"
        cache[release_id] = metadata_str

    with codecs.open ("metadata.json", "wb", "utf-8") as f:
        f.write (metadata_str)

    metadata = json.loads (metadata_str)
    title = metadata["title"]

    if "artist-credit" in metadata:
        title = artist_name (metadata["artist-credit"]) + " - " + title

    print ("Saved metadata for", title)


dirs = ["."]
if len (sys.argv) == 2:
    m = re.match('https://musicbrainz.org/release/(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})', sys.argv[1])
    if m:
        download_metadata (m.group(1))
        sys.exit (0)

if len (sys.argv) > 1:
    dirs = sys.argv[1:]

dirs = list (map (os.path.realpath, dirs))

mbid_results = collections.OrderedDict()

for current_dir in dirs:
    if os.path.isdir (current_dir):
        os.chdir (current_dir)
        for root, dirs, files in os.walk (current_dir):
            result = mbid.mbid_for_dir (root)
            if not result:
                continue
            else:
                mbid_results[root] = result

warnings = []
for current_dir, mbid_result in mbid_results.items():
    os.chdir (current_dir)
    (release_id, discno) = mbid_result

    if not release_id:
        warnings.append ("WARNING No mbid found for %s" % current_dir)
        continue

    download_metadata (release_id)


if warnings:
    print ("\n".join (warnings))
