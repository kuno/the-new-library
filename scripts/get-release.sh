#!/bin/sh

# MBID=10ed5ae4-2bb8-48ac-89ac-a5ea58667f44

curl "https://musicbrainz.org/ws/2/release/$MBID?fmt=json&inc=artist-credits+media+recordings+discids+isrcs" | jq . > "metadata.json"
echo "Saved to metadata.json"

