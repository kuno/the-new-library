#!/usr/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import json
import mbid
import os
import os.path
import requests
import sys

arguments = ["."]
if len (sys.argv) > 1:
    arguments = sys.argv[1:]

arguments = list (map (os.path.realpath, arguments))

maxdisc = {}

for current_arg in arguments:
    if not os.path.isdir (current_arg):
        continue

    for root, dirs, files in os.walk (current_arg):
        result = mbid.mbid_for_dir (root)
        if result:
            print (root, result.mbid, result.discno)

            key = os.path.dirname (root)
            if not key in maxdisc:
                maxdisc[key] = []

            maxdisc[key].append((os.path.basename (root), result.discno))

for key, discs in maxdisc.items():
    padding = 1
    if len(discs) > 9:
        padding = 2
    if len(discs) > 99:
        padding = 3

    for d in discs:
        oldname = d[0]
        newname = "disc" + str(d[1]).zfill(padding)

        if oldname == newname:
            continue

        os.chdir (key)
        os.rename (oldname, newname)
        context = os.path.basename (key)
        print ('Under "%s", renamed "%s" to "%s"' % (context, oldname, newname))
