#!/usr/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import datetime
import json
import math
import mbid
import os
import os.path
import re
import requests
import sys
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4

archive_root = "/mnt/pucca/annex/music.archive/"
library_root = "/mnt/zooyork/annex/library/"
old_library_root = "/mnt/gavroche/annex/music/"

def human_time(seconds):
    units = [("wk", 604800), ("d", 86400), ("h", 3600), ("m", 60), ("s", 1)]
    parts = []
    for unit, mul in units:
        if seconds / mul >= 1 or mul == 1:
            if mul > 1:
                n = int(math.floor(seconds / mul))
                seconds -= n * mul
            else:
                n = int(seconds)
            parts.append("%s%s" % (n, unit))
    return " ".join(parts)

def mp3_duration (filename):
    m = MP3 (filename)
    return m.info.length

def mp4_duration (filename):
    m = MP4 (filename)
    return m.info.length


def duration_report ():
    total_duration = 0

    for root, dirs, files in os.walk (library_root):
        files = [f for f in files if not f[0] == '.']
        dirs[:] = [d for d in dirs if not d[0] == '.']

        for filename in files:
            duration = None
            m = re.match("^disc([0-9]+).m4a$", filename)
            if m:
                duration = mp4_duration (os.path.join (root, filename))
            else:
                m = re.match("^disc([0-9]+).mp3$", filename)
                if m:
                    duration = mp3_duration (os.path.join (root, filename))

            if duration:
                total_duration += duration

    print ("library duration:\t", human_time(total_duration))


def count_artists (artist_mbids, data):
    if "artist-credit" in data:
        for ac in data["artist-credit"]:
            artist_mbids[ac["artist"]["id"]] = True

    for disc in data["media"]:
        for track in disc["tracks"]:
            if "artist-credit" in track:
                for ac in track["artist-credit"]:
                    artist_mbids[ac["artist"]["id"]] = True


def count_tracks (data):
    count = 0

    for disc in data["media"]:
        count += len (disc["tracks"])

    return count


def entity_count_report ():
    release_count = 0
    track_count = 0
    artist_mbids = {}

    for root, dirs, files in os.walk (library_root):
        files = [f for f in files if not f[0] == '.']
        dirs[:] = [d for d in dirs if not d[0] == '.']

        if "metadata.json" in files:
            with codecs.open (os.path.join (root, "metadata.json"), "rb", "utf-8") as f:
                data = json.loads (f.read ())

                count_artists (artist_mbids, data)
                track_count += count_tracks (data)

            release_count += 1

    artist_count = len(artist_mbids.keys())
    print ("artist count:     \t", artist_count)
    print ("release count:    \t", release_count)
    print ("track count:      \t", track_count)


def missing_files_report ():
    library_ignore = []
    library_ignore_filename = os.path.join (archive_root, '.ludignore')
    if os.path.isfile (library_ignore_filename):
        with codecs.open (library_ignore_filename, "rb", "utf-8") as f:
            library_ignore = [line.rstrip() for line in f]

    missing_from_library = []
    for root, dirs, files in os.walk (archive_root):
        files = [f for f in files if not f[0] == '.']
        dirs[:] = [d for d in dirs if not d[0] == '.']

        for f in files:
            if not f.endswith (".tar"):
                continue

            base_filename = os.path.splitext(f)[0]

            base_album = os.path.join (root.replace (archive_root, ''), base_filename)
            library_album = os.path.join (library_root, base_album)
            if (not os.path.isdir (library_album)
                and not base_album in library_ignore):
                missing_from_library.append (base_album)

    archive_ignore = []
    archive_ignore_filename = os.path.join (library_root, '.ludignore')
    if os.path.isfile (archive_ignore_filename):
        with codecs.open (archive_ignore_filename, "rb", "utf-8") as f:
            archive_ignore = [line.rstrip() for line in f]

    missing_from_archive = []
    for root, dirs, files in os.walk (library_root):
        files = [f for f in files if not f[0] == '.']
        dirs[:] = [d for d in dirs if not d[0] == '.']

        if "metadata.json" not in files:
            continue

        base_album = root.replace (library_root, '') + '.tar'
        archive_album = os.path.join (archive_root, base_album)
        if (not os.path.isfile (archive_album)
            and not base_album in archive_ignore):
            missing_from_archive.append (base_album)

    if len(missing_from_library) > 0:
        print ("Missing from library")
        print ("====================")
        for m in missing_from_library:
            print (m)
        print ("")

    if len(missing_from_archive) > 0:
        print ("Missing from archive")
        print ("====================")
        for m in missing_from_archive:
            print (m)
        print ("")

def library_size_report():
    os.chdir (library_root)
    output = os.popen ("du -h --max-depth=0").read ()
    parts = output.split('\t')
    print ("library size:    \t", parts[0])

def archive_size_report():
    os.chdir (archive_root)
    output = os.popen ("du -h --max-depth=0").read ()
    parts = output.split('\t')
    print ("archive size:    \t", parts[0])

def old_library_size_report():
    os.chdir (old_library_root)
    output = os.popen ("find * -type d | wc --lines").read ()
    print ("not yet added:   \t", output)

missing_files_report()
entity_count_report()
duration_report()
library_size_report()
archive_size_report()
old_library_size_report()
