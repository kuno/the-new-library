#!/bin/sh

DST=disc1
if [[ `pwd` =~ ^.*/(disc[0-9]+)$ ]]; then
    DST=${BASH_REMATCH[1]}
fi

ls *m4a | sed 's/^/faad "/' | sed 's/$/"/' | sh
rename -v 's/^([0-9]*)\..*.wav/$1.wav/' *.wav
ls *.wav  | sed "s/^/file '/" | sed "s/$/'/" > list.txt
shntool cue *.wav > "$DST.cue"
shntool len *.wav > shntool.len
ffmpeg -f concat -i list.txt -vn -strict experimental -acodec aac -b:a 256k "$DST.m4a"
