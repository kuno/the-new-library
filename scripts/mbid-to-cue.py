#!/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1, see LICENSE.txt

import codecs
import json
import re
import sys
import unicodedata

version = 1

def ms_to_msf (ms):
    "milliseconds to minute:second:frame format"
    frames_in_ms = ms % 1000
    tmp = ms / 1000
    seconds = tmp % 60
    tmp /= 60
    minutes = tmp

    frames = frames_in_ms / (1000.0 / 75)

    return "%d:%02d:%02d" % (minutes, seconds, int(round((frames))))


def slug (str):
    # FIXME: use https://pypi.python.org/pypi/python-slugify
    slug = unicodedata.normalize('NFKD', str)
    slug = slug.encode('ascii', 'ignore').lower().decode('ascii')
    slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
    return re.sub(r'[-]+', '-', slug)


def escape_for_cue (str):
    # cue sheets don't support any escaping, so replace double quotes with single quotes.
    return str.replace ('"', "'")


def artist_name (ac):
    name = []
    for part in ac:
        name.append (part["name"])
        name.append (part["joinphrase"])

    return "".join (name)


def build_cue_track (trackno, offset, track, data):
    output = [
        "  TRACK %02d AUDIO" % (trackno)
    ]

    if "artist-credit" in track:
        output.append ('    PERFORMER "'
                       + escape_for_cue (artist_name(track["artist-credit"])) + '"')

    track_title = ""
    if 'title' in track:
        track_title = track['title']
    elif "title" in track['recording']:
        track_title = track['recording']['title']

    output.append ('    TITLE "' + escape_for_cue (track_title) + '"')

    # output.append ("    INDEX 01 " + str (offset))
    output.append ("    INDEX 01 " + ms_to_msf (offset))

    return output

def build_cue (track_lengths, data, disc_idx):
    output = [
        "REM COMMENT mbid-to-cue v" + str (version)
    ]

    # FIXME: discids
    year = None

    if "release-events" in data:
        dates = []

        for event in data["release-events"]:
            if "date" in event:
                dates.append (event["date"])

        dates.sort ()
        if len (dates):
            year_re = re.compile ('[0-9][0-9][0-9][0-9]')
            if year_re.match(dates[0]):
                year = dates[0][0:4]

            output.append ("REM DATE " + dates[0])

    if "artist-credit" in data:
        output.append ('PERFORMER "' + escape_for_cue (artist_name(data["artist-credit"])) + '"')

    output.append ('TITLE "' + escape_for_cue (data["title"]) + '"')
    output.append ('REM MUSICBRAINZ_ALBUM_ID ' + data["id"]);

    filename = []
    if year:
        filename.append (year)

    output.append ('FILE "disc1.m4a" WAVE')

    if "media" in data:
        current_disc = -1
        for disc in data["media"]:
            current_disc += 1
            if current_disc != disc_idx:
                continue;

            trackno = 0
            offset = 0
            for track in disc["tracks"]:
                trackno += 1
                output.extend (build_cue_track (trackno, offset, track, data))
                if "length" in track:
                    if track_lengths:
                        offset += track_lengths[trackno-1]
                    else:
                        offset += track["length"]

    return output

track_lengths = None
try:
    with codecs.open ("shntool.len", "rb", "utf-8") as f:
        track_lengths = []
        shntool_lengths = f.readlines ()
        for line in shntool_lengths:
            frames = re.compile ('\s+([0-9]*):([0-9][0-9])\.([0-9][0-9])\s+')
            match = frames.match (line)
            if match:
                m = match.group(1)
                s = match.group(2)
                f = match.group(3)
                l = (int (m) * 60 + int (s)) * 1000 + (int (f) * 1000.0/75)
                track_lengths.append (int(round(l)))
                continue

            milliseconds = re.compile ('\s+([0-9]*):([0-9][0-9])\.([0-9][0-9][0-9])\s+')
            match = milliseconds.match (line)
            if match:
                m = match.group(1)
                s = match.group(2)
                n = match.group(3)
                l = (int (m) * 60 + int (s)) * 1000 + int (n)
                track_lengths.append (l)
except:
    errmsg = "No shntool.len file found, using track offsets from MusicBrainz"
    # FIXME: print above error message to stderr


disc_idx = 0
if len(sys.argv) == 2:
    disc_idx = int(sys.argv[1]) - 1

with codecs.open ("metadata.json", "rb", "utf-8") as f:
    data = json.loads (f.read ())
    cue_file = "\n".join (build_cue (track_lengths, data, disc_idx))
    print (cue_file)

