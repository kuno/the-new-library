#!/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import json
import os
import re
import sys

version = 1

def ms_to_msf (ms):
    "milliseconds to minute:second:frame format"
    frames_in_ms = ms % 1000
    tmp = ms / 1000
    seconds = tmp % 60
    tmp /= 60
    minutes = tmp

    frames = frames_in_ms / (1000.0 / 75)

    return "%d:%02d:%02d" % (minutes, seconds, int(round((frames))))


def escape_for_cue (str):
    # cue sheets don't support any escaping, so replace double quotes with single quotes.
    return str.replace ('"', "'")


def artist_name (ac):
    name = []
    for part in ac:
        name.append (part["name"])
        name.append (part["joinphrase"])

    return "".join (name)


def context_add (data, cmd, args):
    if data["_current"]:
        if not cmd in data["tracks"][data["_current"]]:
            data["tracks"][data["_current"]][cmd] = []
        data["tracks"][data["_current"]][cmd].append(args)
    else:
        if not cmd in data:
            data[cmd] = []
        data[cmd].append(args)

    return data

def context_set (data, cmd, args):
    if data["_current"] != None:
        data["tracks"][data["_current"]][cmd] = args
    else:
        data[cmd] = args

    return data

def parse_line (line, data):
    line = line.strip ()
    if line == "":
        return data

    elif line.startswith ("REM COMMENT lûd cue v") or line.startswith ("REM COMMENT mbid-to-cue"):
        return data

    elif line.startswith ("REM MUSICBRAINZ_ALBUM_ID "):
        parts = line.split (' ')
        data["mbid"] = parts[1]

    elif line.startswith ("REM"):
        parts = line.split (' ', maxsplit=1)
        data = context_add (data, "REM", parts[1])

    elif (line.startswith ("PERFORMER")
        or line.startswith ("SONGWRITER")
        or line.startswith ("CATALOG")
        or line.startswith ("TITLE")):
        parts = line.split (' ', maxsplit=1)
        value = parts[1].replace('"', '')
        data = context_set (data, parts[0], value)

    elif line.startswith ("FILE"):
        parts = line.split (' ', maxsplit=1)
        args = parts[1].rsplit (' ', maxsplit=1)
        value = args[0].replace('"', '')
        if "FILE" in data:
            print ("FILE already specified, only one instance of FILE allowed per .cue file")
            sys.exit (1)
        data["FILE"] = {'filename': value, 'filetype': args[1]}

    elif line.startswith ("TRACK"):
        parts = line.split (' ', maxsplit=2)
        if data["_current"] == None:
            data["_current"] = 0
        else:
            data["_current"] += 1

        data["tracks"].append({"datatype": parts[2], "indexes": []})
        # pprint.pprint (data["_current"])

    elif line.startswith ("INDEX"):
        parts = line.split (' ', maxsplit=2)
        data["tracks"][data["_current"]]["indexes"].append({
            'pos': int(parts[1]), 'start': parts[2]});

    elif (line.startswith ("FLAGS")
          or line.startswith ("PREGAP")
          or line.startswith ("POSTGAP")
          or line.startswith ("ISRC")):
        parts = line.split (' ', maxsplit=1)
        value = parts[1].replace('"', '')
        data["tracks"][data["_current"]][parts[0]] = value

    else:
        print ("Unrecognized command", line)

    return data

def parse_cue (f):
    lines = f.readlines ()
    data = { "_current": None, "tracks": [] }
    for line in lines:
        data = parse_line (line, data)

    return data

def render_cue (data):
    ret = []

    root_level = ["CATALOG", "CDTEXTFILE", "FILE"]
    any_level = ["REM", "PERFORMER", "SONGWRITER", "TITLE"]

    ret.append("REM COMMENT lûd cue v" + str(version))

    if "REM" in data:
        for line in data["REM"]:
            ret.append ("REM " + line)

    if "mbid" in data:
        ret.append ('REM MUSICBRAINZ_ALBUM_ID ' + data["mbid"])

    if "CATALOG" in data and data["CATALOG"]:
        ret.append ("CATALOG " + data["CATALOG"])

    if "TITLE" in data:
        ret.append ('TITLE "' + data["TITLE"] + '"')

    if "PERFORMER" in data:
        ret.append ('PERFORMER "' + data["PERFORMER"] + '"')

    if "SONGWRITER" in data:
        ret.append ('SONGWRITER "' + data["SONGWRITER"] + '"')

    if "CDTEXTFILE" in data:
        ret.append ("CDTEXTFILE " + data["CDTEXTFILE"])

    if "FILE" in data:
        ret.append ('FILE "' + data["FILE"]["filename"] + '" ' + data["FILE"]["filetype"])


    idx = 0
    for track in data["tracks"]:
        idx += 1
        ret.append ('  TRACK %02d %s' % (idx, track['datatype']))

        if "REM" in track:
            for line in track["REM"]:
                ret.append ('    REM ' + line)

        if "TITLE" in track:
            ret.append ('    TITLE "%s"' % track["TITLE"])

        if "PERFORMER" in track:
            ret.append ('    PERFORMER "%s"' % track["PERFORMER"])

        if "SONGWRITER" in track:
            ret.append ('    SONGWRITER "%s"' % track["SONGWRITER"])

        if "FLAGS" in track:
            ret.append ('    FLAGS %s' % track['FLAGS'])

        if "ISRC" in track:
            ret.append ('    ISRC %s' % track['ISRC'])

        if "PREGAP" in track:
            ret.append ('    PREGAP %s' % track['PREGAP'])

        if "indexes" in track:
            for index in track["indexes"]:
                ret.append ('    INDEX %02d %s' % (index['pos'], index['start']))

        if "POSTGAP" in track:
            ret.append ('    POSTGAP %s' % track['POSTGAP'])

    return "\n".join (ret) + "\n"


def update_cue_track (idx, cue_data, mb_data):
    if idx >= len(cue_data["tracks"]):
        return

    if "artist-credit" in mb_data:
        cue_data["tracks"][idx]["PERFORMER"] = escape_for_cue (
            artist_name(mb_data["artist-credit"]))

    track_title = ""
    if 'title' in mb_data:
        track_title = mb_data['title']
    elif "title" in mb_data['recording']:
        track_title = mb_data['recording']['title']

    cue_data["tracks"][idx]["TITLE"] = track_title

    # preserve existing ISRC if available, as a musicbrainz recording may be associated
    # with multiple ISRCs, and we're just picking one of them at random.  In most cases
    # recordings will only have one ISRC, so that should be fine -- but if there are
    # multiple probably whatever we have is more accurate for the release.
    if ("isrcs" in mb_data["recording"]
        and len(mb_data["recording"]["isrcs"])
        and not "ISRC" in cue_data["tracks"][idx]):
        cue_data["tracks"][idx]["ISRC"] = mb_data["recording"]["isrcs"][0]


def update_cue (discno, ext, cue_data, mb_data):
    if "artist-credit" in mb_data:
        cue_data["PERFORMER"] = escape_for_cue (artist_name(mb_data["artist-credit"]))

    cue_data["mbid"] = mb_data["id"]
    cue_data["TITLE"] = escape_for_cue (mb_data["title"])

    if "media" in mb_data:
        pos = int(discno) - 1
        if pos < len(mb_data["media"]):
            disc = mb_data["media"][pos]

            for i, track in enumerate(disc["tracks"]):
                update_cue_track (i, cue_data, track)

    if "barcode" in mb_data:
        cue_data["CATALOG"] = mb_data["barcode"]

    if not "FILE" in cue_data:
        cue_data["FILE"] = {'filetype': 'WAVE'}

    cue_data["FILE"]["filename"] = "disc%s%s" % (discno, ext)


def generate_cue (f):
    track_lengths = []
    shntool_lengths = f.readlines ()
    for line in shntool_lengths:
        frames = re.compile ('\s+([0-9]*):([0-9][0-9])\.([0-9][0-9])\s+')
        match = frames.match (line)
        if match:
            m = match.group(1)
            s = match.group(2)
            f = match.group(3)
            l = (int (m) * 60 + int (s)) * 1000 + (int (f) * 1000.0/75)
            track_lengths.append (int(round(l)))
            continue

        milliseconds = re.compile ('\s+([0-9]*):([0-9][0-9])\.([0-9][0-9][0-9])\s+')
        match = milliseconds.match (line)
        if match:
            m = match.group(1)
            s = match.group(2)
            n = match.group(3)
            l = (int (m) * 60 + int (s)) * 1000 + int (n)
            track_lengths.append (l)


    trackno = 0
    offset = 0
    tracks = []
    pos = 1
    tracks.append({
        'indexes': [
            {
                'pos':pos,
                'start': ms_to_msf (offset),
            }
        ],
        'datatype': 'AUDIO',
    })

    for t in track_lengths[:-1]:
        pos += 1
        offset += t
        tracks.append({
            'indexes':[
                {
                    'pos':pos,
                    'start': ms_to_msf (offset)
                }
            ],
            'datatype': 'AUDIO',
        })

    return {'tracks': tracks}


dirs = ["."]
if len (sys.argv) > 1:
    dirs = sys.argv[1:]

dirs = list (map (os.path.realpath, dirs))

warnings = []
for current_dir in dirs:
    if os.path.isdir (current_dir):
        os.chdir (current_dir)

        metadata = None
        with codecs.open ("metadata.json", "rb", "utf-8") as f:
            metadata = json.loads (f.read())

        files = os.listdir ()

        files.sort ()

        for filename in files:
            m = re.match("^disc([0-9]+).cue$", filename)
            if not m:
                continue

            data = None
            discno = m.group(1)

            ext = ".m4a"
            for audiofile in files:
                if audiofile.startswith("disc%s." %discno):
                    ext = os.path.splitext(audiofile)[1]

            try:
                with codecs.open (filename, "rb", "utf-8") as f:
                    data = parse_cue (f)
            except UnicodeDecodeError:
                # so it's not UTF-8, probably latin1 or latin9 then.
                with codecs.open (filename, "rb", "ISO-8859-15") as f:
                    data = parse_cue (f)

            if not data['tracks']:
                # cue is empty, try to generate it from shntool.len instead
                with codecs.open ("shntool.len", "rb", "utf-8") as f:
                    data = generate_cue (f)

            if not data['tracks']:
                warnings.append("WARNING: .cue is empty in %s" % current_dir)

            update_cue (discno, ext, data, metadata)
            new_cue = render_cue (data)

            os.rename (filename, filename + "~")
            with codecs.open (filename, "wb", "utf-8") as f:
                f.write (new_cue)

            print ("Updated", filename)

if warnings:
    print ("\n".join (warnings))
