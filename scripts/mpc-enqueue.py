#!/bin/env python3

# This file is part of "The New Library".
# Copyright 2016  Kuno Woudt <kuno@frob.nl>.
#
# "The New Library" is free software; you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1

import codecs
import json
import os
import os.path
import re
import shlex
import sys

music_root = os.path.expanduser("~/music")
music_root = "/mnt/zooyork/annex/library/"
music_url = "https://18-volt.frob.mobi/m593/"

def find_dir (dirname):
    if os.path.exists (dirname):
        return os.path.realpath (dirname)

    attempt = os.path.join (music_root, dirname)
    if os.path.exists (attempt):
        return os.path.realpath (attempt)

    files = os.listdir (music_root)
    for f in files:
        if os.path.exists (f):
            attempt = os.path.join (music_root, f, dirname)
            if os.path.exists (attempt):
                return os.path.realpath (attempt)

    print ("WARNING:", dirname, "not found")

    return None


to_queue = ["."]
if len (sys.argv) > 1:
    to_queue = sys.argv[1:]

to_queue = list (map (find_dir, to_queue))

queue = []
for d in to_queue:
    if not d:
        continue

    elif os.path.isdir (d):
        for root, dirs, files in os.walk (d):
            # Skip hidden files and directories.
            files = [f for f in files if not f[0] == '.']
            dirs[:] = [d for d in dirs if not d[0] == '.']

            for f in sorted(files):
                if re.match("disc[0-9]*.cue", f):
                    queue.append(os.path.join (root, f))

    elif os.path.isfile (d):
        queue.append(d)

for item in queue:
    mpc_path = item.replace(music_root, '')

    output = os.popen ("mpc load " + shlex.quote (mpc_path)).read ()
    print (output, end='')
