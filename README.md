
Lûd Requirements
====================

    # audio conversion scripts
    sudo dnf install faad2 ffmpeg flac lame python3-requests shntool vorbis-tools
    # FIXME: Larry Wall's rename needs to be in the path, but it is not packaged for Fedora

    # web player indexer and queue management
    sudo dnf install couchdb python3-couchdb

Web Player requirements
=======================

    wget https://rpm.nodesource.com/setup_6.x
    sudo bash setup_6.x
    sudo dnf install nodejs
    npm install

License
=======

copyright 2016 [Kuno Woudt](https://frob.nl/#me), licensed under copyleft-next 0.3.1.


